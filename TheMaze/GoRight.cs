﻿using Assets.Scripts;
using System;
using System.Collections.Generic;
using System.Linq;

//Change only the class name
public class GoRight : IPlayerAI
{
    private string TeamName = "[GOR]";

    private CharacterType TeamCharacter = CharacterType.Any;

    public DirectionType RequestMove(DirectionType[] possibleDirections)
    {
        return DirectionType.Right;
    }
}

