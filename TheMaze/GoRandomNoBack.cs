﻿using Assets.Scripts;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

public class GoRandomNoBack : IPlayerAI
{
    private string TeamName = "[RND]";

    private CharacterType TeamCharacter = CharacterType.Elvis;
    private DirectionType lastMove;
    private readonly Random random = new Random();

    public DirectionType RequestMove(DirectionType[] possibleDirections)
    {
        return lastMove = GetDirectionNoBack(possibleDirections);
    }

    private DirectionType GetDirectionNoBack(DirectionType[] possibleDirections)
    {
        var newMove = GetRandomDirection(possibleDirections);

        if (possibleDirections.Length > 1)
        {
            var oppositeDirection = GetOppositeDirection(newMove);
            if (oppositeDirection == lastMove)
            {
                var newList = possibleDirections.ToList();
                newList.Remove(newMove);
                return GetRandomDirection(newList.ToArray());
            }
        }
        return newMove;
    }

    private DirectionType GetRandomDirection(DirectionType[] possibleDirections) => possibleDirections[random.Next(possibleDirections.Length)];

    private static DirectionType GetOppositeDirection(DirectionType direction)
    {
        switch (direction)
        {
            case DirectionType.Left:
                return DirectionType.Right;
            case DirectionType.Up:
                return DirectionType.Down;
            case DirectionType.Right:
                return DirectionType.Left;
            case DirectionType.Down:
                return DirectionType.Up;
            default:
                throw new InvalidEnumArgumentException();
        }
    }
}
