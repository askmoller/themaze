﻿using Assets.Scripts;
using System;
using System.Collections.Generic;
using System.Linq;

//Change only the class name
public class GoRandom : IPlayerAI
{
    private string TeamName = "[RND]";

    private CharacterType TeamCharacter = CharacterType.Any;

    public DirectionType RequestMove(DirectionType[] possibleDirections)
    {
        return possibleDirections[new System.Random().Next(possibleDirections.Length)];
    }
}

