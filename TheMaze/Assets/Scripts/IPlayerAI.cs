﻿namespace Assets.Scripts
{
    //Don't touch it.
    public interface IPlayerAI
    {
        //Don't touch it.
        DirectionType RequestMove(DirectionType[] direction);
    }
}
