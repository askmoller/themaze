﻿//Don't touch it
namespace Assets.Scripts
{
    //Don't touch it
    public enum CharacterType
    {
        YourCharacterType,
        Doctor,
        Elvis,
        EvilDoc,
        Joker,
        Renegate,
        Scientist,
        Scout,
        Teacher,
        Vader,
        VanDame,
        Any //This option means that you do not care about how your code will look like.
    }
}
