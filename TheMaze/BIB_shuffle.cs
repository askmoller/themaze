﻿using Assets.Scripts;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

//Change only the class name
public class BIB_shuffle : IPlayerAI
{
    private string TeamName = "[BIB]";
    private CharacterType TeamCharacter = CharacterType.Elvis;


    private List<DirectionType> _oldSteps = new List<DirectionType>();
    private DirectionType lastMove;
    private bool inversedOrder = false;
    private DirectionType[] directionOrder = new DirectionType[] { DirectionType.Left, DirectionType.Down, DirectionType.Right, DirectionType.Up };
    public DirectionType RequestMove(DirectionType[] possibleDirections)
    {
        var newMove = GetDirectionNoBack(possibleDirections);

        var totalSteps = _oldSteps.Count;
        if (totalSteps > 40)
        {
            directionOrder = Shuffle(directionOrder);
            _oldSteps.Clear();
        }
        //var repeatCount = 20;
        //if (totalSteps > repeatCount * 3 + 1)
        //{
        //    var currentCycle = _oldSteps.GetRange(totalSteps - repeatCount, repeatCount);

        //    for (int i = 1; i < repeatCount * 2; i++)
        //    {
        //        var oldCycle = _oldSteps.GetRange(totalSteps - repeatCount - i, repeatCount);

        //        if (Enumerable.SequenceEqual(currentCycle, oldCycle))
        //        {
        //            directionOrder = Shuffle(directionOrder);
        //            _oldSteps.Clear();
        //            break;
        //            //var newDirections = possibleDirections.Where(x => x != newMove).ToArray();
        //            //if (newDirections != null && newDirections.Length > 0)
        //            //    return RequestMove(newDirections);
        //        }
        //    }
        //}

        _oldSteps.Add(newMove);
        return lastMove = newMove;
    }
    private DirectionType GetDirectionNoBack(DirectionType[] possibleDirections)
    {
        var newMove = GetDirection(possibleDirections);

        if (possibleDirections.Length > 1)
        {
            var oppositeDirection = GetOppositeDirection(newMove);
            if (oppositeDirection == lastMove)
            {
                var newList = possibleDirections.ToList();
                newList.Remove(newMove);
                return GetDirection(newList.ToArray());
            }
        }
        return newMove;
    }

    private DirectionType[] Shuffle(DirectionType[] array)
    {
        var random = new Random();
        int n = array.Length;
        while (n > 1)
        {
            n--;
            int k = random.Next(n + 1);
            DirectionType value = array[k];
            array[k] = array[n];
            array[n] = value;
        }
        return array;
    }
    private DirectionType GetDirection(DirectionType[] possibleDirections)
    {
        foreach (var direction in directionOrder)
        {
            if (possibleDirections.Any(x => x == direction))
                return direction;
        }
        return possibleDirections[0];
    }

    private static DirectionType GetOppositeDirection(DirectionType direction)
    {
        switch (direction)
        {
            case DirectionType.Left:
                return DirectionType.Right;
            case DirectionType.Up:
                return DirectionType.Down;
            case DirectionType.Right:
                return DirectionType.Left;
            case DirectionType.Down:
                return DirectionType.Up;
            default:
                throw new InvalidEnumArgumentException();
        }
    }
}

