﻿using System;
using System.Drawing;

namespace MazeRunner
{
    public class ConsoleGraphics
    {
        public int TopStartPos { get; set; }
        public int LeftStartPos { get; set; }
        public ConsoleGraphics(int topStartPos, int leftStartPos)
        {
            TopStartPos = topStartPos;
            LeftStartPos = leftStartPos;
        }
        public void DrawMaze(string[] maze)
        {
            for (int rowNumber = 0; rowNumber < maze.Length; rowNumber++)
            {
                string row = maze[rowNumber];
                for (int columnNumber = 0; columnNumber < row.Length; columnNumber++)
                {
                    char symbol = row[columnNumber];
                    Console.SetCursorPosition(LeftStartPos + columnNumber * 2, TopStartPos + rowNumber * 1);

                    switch (symbol)
                    {
                        case '#':
                            Console.ForegroundColor = ConsoleColor.DarkGray;
                            Console.Write("##");
                            break;
                        case '-':
                            Console.Write("--");
                            break;
                        case 'g':
                            Console.ForegroundColor = ConsoleColor.DarkGreen;
                            Console.Write("gg");
                            break;
                        case 's':
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.Write("ss");
                            break;
                        default:
                            throw new ArgumentException($"Unknown symbol: '{symbol}'");
                    }

                    Console.ResetColor();
                    //Console.SetCursorPosition(leftStartPos + columnNumber * 2, topStartPos + rowNumber * 1);
                    //Console.Write(symbolToWrite);
                    //Console.SetCursorPosition(leftStartPos + columnNumber * 2, topStartPos + rowNumber * 2 + 1);
                    //Console.Write(symbolToWrite);
                }
            }
        }
        public void DrawPlayer(Point position)
        {
            Console.SetCursorPosition(LeftStartPos + position.X*2, TopStartPos + position.Y);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("<>");
            Console.ResetColor();

        }
        public void ClearPlayerDrawing(string[] maze, Point position)
        {
            var symbol = maze[position.Y][position.X];

            Console.SetCursorPosition(LeftStartPos + position.X * 2, TopStartPos + position.Y);
            switch (symbol)
            {
                case '#':
                    Console.ForegroundColor = ConsoleColor.DarkGray;
                    Console.Write("##");
                    break;
                case '-':
                    Console.Write("--");
                    break;
                case 'g':
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.Write("gg");
                    break;
                case 's':
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.Write("ss");
                    break;
                default:
                    throw new ArgumentException($"Unknown symbol: '{symbol}'");
            }
            Console.ResetColor();
        }
    }
}
