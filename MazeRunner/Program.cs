﻿using Assets.Scripts;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace MazeRunner
{
    class Program
    {
        private static int updateInterval; // update interval in ms
        private static bool isRunning;
        private static bool isPaused;
        private static bool skipMaze;
        private static readonly object lockObject = new object();
        static void Main(string[] args)
        {
            // This can be changed to modify the speed of the game loop. It is measured in ms. Recommended values are between: 10 (very fast) - 300 (easy to follow the steps).
            updateInterval = 100;

            // Add the type with the player algorithm here. It must implement IPlayerAI and remove all references to the unity framework.
            var playerAlgorithm = GetType<BIB>();

            // Start the game loop.
            GameLoop(playerAlgorithm);
        }
        private static Type GetType<T>() where T : IPlayerAI
        {
            return typeof(T);
        }
        private static void GameLoop(Type playerAlgorithm)
        {
            var consoleGraphics = new ConsoleGraphics(5, 4);
            Console.CursorVisible = false;

            foreach (var item in Mazes.Collection)
            {
                // Initialize the game
                var playerAI = (IPlayerAI)Activator.CreateInstance(playerAlgorithm);
                var playerPosition = new PlayerPosition(item.maze);
                int steps = 0;

                Console.WriteLine($"Maze name: {item.name}");
                Console.WriteLine($"Player AI: {playerAlgorithm}");
                ClearAndWriteLine($"Current steps: {steps}");
                ClearAndWriteLine($"Update interval (ms): {updateInterval}");

                consoleGraphics.DrawMaze(item.maze);
                Console.WriteLine("\n\nPress any key to start");
                PrintCommands();
                Console.ReadKey();

                // Start the game
                ClearAndWriteLine("Game is running...", item.maze.Length + 6);
                isRunning = true;
                playerPosition.SetStartPosition();
                consoleGraphics.DrawPlayer(playerPosition.Position);

                var gameTask = Task.Run(async () =>
                  {
                      while (isRunning)
                      {
                          await Task.Delay(updateInterval);
                          lock (lockObject)
                          {
                              if (isPaused) continue;

                              // Get the direction from the player algorithm
                              var direction = playerAI.RequestMove(playerPosition.GetDirections());

                              // Clear the old player position from the console graphics
                              consoleGraphics.ClearPlayerDrawing(item.maze, playerPosition.Position);

                              // Update the position of the player
                              playerPosition.UpdatePosition(direction);

                              // Draw the updated position of the player on the console graphics
                              consoleGraphics.DrawPlayer(playerPosition.Position);

                              ClearAndWriteLine($"Current steps: {++steps}", 2);
                              ClearAndWriteLine($"Update interval (ms): {updateInterval}");
                              if (playerPosition.IsInWall)
                              {
                                  ClearAndWriteLine($"Player is wall. Player is dead", item.maze.Length + 6);
                                  isRunning = false;
                              }
                              if (playerPosition.IsInGoal)
                              {
                                  ClearAndWriteLine($"Player is goal. Player is win", item.maze.Length + 6);
                                  isRunning = false;
                              }
                          }
                      }
                  });
                while (isRunning)
                {
                    lock (lockObject)
                    {
                        HandleKeyInput(item.maze);
                    }
                }
                gameTask.Wait();
                if (!skipMaze)
                    Console.ReadKey();
                skipMaze = false;
                isPaused = false;
                Console.Clear();
            }
        }
        private static void PrintCommands()
        {
            Console.WriteLine("\nCommands:");
            Console.WriteLine("Enter - Next Maze");
            Console.WriteLine("\"+\" - increase speed");
            Console.WriteLine("\"-\" - decrease speed");
            Console.WriteLine("Spacebar - pause/unpause game");
            Console.WriteLine("Escape - exit application");
        }
        private static void ClearAndWriteLine(string text)
        {
            ClearCurrentConsoleLine();
            Console.WriteLine(text);
        }
        private static void ClearAndWriteLine(string text, int height)
        {
            Console.SetCursorPosition(0, height);
            ClearCurrentConsoleLine();
            Console.WriteLine(text);
        }
        private static void ClearCurrentConsoleLine()
        {
            int currentLineCursor = Console.CursorTop;
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, currentLineCursor);
        }
        private static void HandleKeyInput(string[] maze)
        {
            if (Console.KeyAvailable)
            {
                ConsoleKeyInfo key = Console.ReadKey(true);
                if (key.Key == ConsoleKey.Enter)
                {
                    isRunning = false;
                    skipMaze = true;
                }
                else if (key.Key == ConsoleKey.Escape)
                {
                    Console.SetCursorPosition(0, maze.Length + 6);
                    Environment.Exit(0);
                }
                else if (key.Key == ConsoleKey.Add)
                {
                    updateInterval = updateInterval + 10;
                    ClearAndWriteLine($"Update interval (ms): {updateInterval}", 3);
                }
                else if (key.Key == ConsoleKey.Subtract)
                {
                    updateInterval = updateInterval - 10;
                    if (updateInterval < 10) updateInterval = 10;
                    ClearAndWriteLine($"Update interval (ms): {updateInterval}", 3);
                }
                else if (key.Key == ConsoleKey.Spacebar)
                {
                    isPaused = !isPaused;
                    ClearAndWriteLine(isPaused ? "Game is paused..." : "Game is running...", maze.Length + 6);
                }
            }
        }
    }
}