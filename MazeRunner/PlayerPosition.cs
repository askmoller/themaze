﻿using Assets.Scripts;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;

namespace MazeRunner
{
    public class PlayerPosition
    {
        public string[] Maze { get; }
        public Point Position { get; private set; }
        public PlayerPosition(string[] maze)
        {
            Maze = maze;
        }
        public void SetStartPosition()
        {
            Position = CoordinatesOf('s');
        }
        public void UpdatePosition(DirectionType direction)
        {
            Position = Move(direction);
        }
        public DirectionType[] GetDirections()
        {
            if (!IsPositionValid || IsInWall) return new DirectionType[0];

            var wall = '#';

            List<DirectionType> directionTypes = new List<DirectionType>();

            if (Maze[Position.Y][Position.X - 1] != wall)
            {
                directionTypes.Add(DirectionType.Left);
            }
            if (Maze[Position.Y][Position.X + 1] != wall)
            {
                directionTypes.Add(DirectionType.Right);
            }
            if (Maze[Position.Y - 1][Position.X] != wall)
            {
                directionTypes.Add(DirectionType.Up);
            }
            if (Maze[Position.Y + 1][Position.X] != wall)
            {
                directionTypes.Add(DirectionType.Down);
            }
            return directionTypes.ToArray();
        }
        public bool IsInGoal => IsPositionValid && Maze[Position.Y][Position.X] == 'g';
        public bool IsInWall => !IsPositionValid || Maze[Position.Y][Position.X] == '#';
        private bool IsPositionValid => Position != null && !Position.IsEmpty && Position.X != 0 && Position.Y != 0 && Position.X != Maze.First().Length && Position.Y != Maze.Length;
        private Point CoordinatesOf(char value)
        {
            for (int rowNumber = 0; rowNumber < Maze.Length; rowNumber++)
            {
                string row = Maze[rowNumber];
                for (int columnNumber = 0; columnNumber < row.Length; columnNumber++)
                {
                    char symbol = row[columnNumber];
                    if (symbol == value)
                        return new Point(columnNumber, rowNumber);
                }
            }
            return new Point();
        }
        private Point Move(DirectionType direction)
        {
            var position = Position;
            switch (direction)
            {
                case DirectionType.Left:
                    position.X--;
                    break;
                case DirectionType.Up:
                    position.Y--;
                    break;
                case DirectionType.Right:
                    position.X++;
                    break;
                case DirectionType.Down:
                    position.Y++;
                    break;
                default:
                    throw new InvalidEnumArgumentException();
            }
            return position;
        }
    }
}